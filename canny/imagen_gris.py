#Vision Computacional
#Primer Hemi
#Pamela Barrionuevo

import cv2 as cv
#1.leer
img=cv.imread('./imread/jugador.jpg',0) #accede a archivos mas internos(hacia adentro)
#2. ver
cv.imshow('IMAGEN_GRIS',img);# el IMAGEN es el TITULO
#3. guardar
cv.imwrite("./imwrite/jugador.jpg",img);
cv.waitKey() #espere a que se cierre manualmente
