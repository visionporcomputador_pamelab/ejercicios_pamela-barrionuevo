#Vision Computacional
#Primer Hemi
#Pamela Barrionuevo
import cv2 as cv
#1.leer
img=cv.imread('./imread/conejo.jpg') #accede a archivos mas internos(hacia adentro)
#2.mostar
cv.imshow("Pic", img)
#3.se hace negativa
img_not = cv.bitwise_not(img)
#2.mostar
cv.imshow("Invert1", img_not)
cv.waitKey(0)
cv.destroyAllWindows()