#Vision Computacional
#Primer Hemi
#Pamela Barrionuevo

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import sys
umbral1 = sys.argv[1]
umbral1 = sys.argv[2]

img = cv.imread('./imread/monedas.jpeg',0)
edges = cv.Canny(img,100,100)#alto, bajo

plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()