# Algoritmo de deteccion de colores multiples, POR CANALES

import cv2
import numpy as np
bgr = cv2.imread('./imread/loro.png')
C1 = bgr[:,:,0]
C2 = bgr[:,:,1]
C3 = bgr[:,:,2]
#cv2.imshow('BGR',np.hstack([C1,C2,C3]))
cv2.imshow('Original',bgr)
rgb = cv2.cvtColor(bgr,cv2.COLOR_BGR2RGB)
C1 = rgb[:,:,0]
C2 = rgb[:,:,1]
C3 = rgb[:,:,2]
cv2.imshow('R',np.hstack([C1]))
cv2.imwrite("./imwrite/R.png",C1);
cv2.imshow('G',np.hstack([C2]))
cv2.imwrite("./imwrite/G.png",C2);
cv2.imshow('B',np.hstack([C3]))
cv2.imwrite("./imwrite/B.png",C3);
cv2.waitKey(0)
cv2.destroyAllWindows()
